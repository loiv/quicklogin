package pl.onyxcherry.loiv.quicklogin;

import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraftforge.client.event.ClientPlayerNetworkEvent.LoggedInEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = "quicklogin")
public class LoggedInListener {

    @SubscribeEvent
    public static void onConnect(LoggedInEvent event) {
        ClientPlayerEntity player = event.getPlayer();
        String remote_server_addr = player.connection.getConnection().getRemoteAddress().toString();
        if (remote_server_addr.startsWith("minecraft.loiv.torun.pl") && remote_server_addr.contains("25565")) {
            String myPassword = Config.MY_SERVER_PASSWORD.get();
            player.chat("/login " + myPassword);
        }
    }
}
