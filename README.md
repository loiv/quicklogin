# QuickLogin

## Forge 1.16 mod to quick log in to the server

## Build

Build with Gradle (e.g. `./gradlew build`)

## Config

After first run (mod load), go to `.minecraft/config/quicklogin-client.toml` and edit `password`.
